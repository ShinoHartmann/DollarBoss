package com.gsm.project.accmgmt.txparser.reader;

import com.gsm.project.accmgmt.txparser.BankStatement;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Nationwide specific debit statement reader.
 */
@Component
public class NationwideDebitReader extends Reader {

    public static final String TYPE = "Nationwide";

    public NationwideDebitReader() {
        super(TYPE);
    }

    /**
     * @see Reader#readFile
     */
    @Override
    public void readFile(int userId, List<String> inputLines, List<BankStatement> list) throws Exception{
        validateStatement(inputLines);
        String account = null;
        BankStatement stm;
        for(String line : inputLines) {
            String[] lineElement = line.replaceAll("[\"£\\r]", "").split(",");
            if(lineElement[0].equals("")) {
                continue;
            } else if(lineElement[0].startsWith("Account Name:")) {
                account = extractAccount(lineElement[1]);
            }
            if(Character.isDigit(lineElement[0].charAt(1))) {
                stm = new BankStatement();
                stm.setUserId(userId); // userId
                stm.setAccountNumber(account); // account
                stm.setDate(formatDate(lineElement[0], "dd MMM yyyy")); //date
                String transactionType = lineElement[1];
                String payOut = lineElement[3];
                String payIn = lineElement[4];
                if(payIn.equals("")) {
                    stm.setAmount("-" + payOut); // amount(out)
                } else {
                    stm.setAmount(payIn);  //amount(in)
                }
                String description = lineElement[2];
                if(!transactionType.equals(description)) {
                    description = transactionType + " " + description;
                }
                stm.setDescription(String.format("%s %s on %s", description, stm.getDate(), stm.getAmount())); // description
                stm.setBalance(lineElement[5]); //balance
                stm.setTransactionCategory(getTransactionCategory(userId, stm)); // transactionCategory
                list.add(stm);
            }
        }
    }

    /**
     * Validates whether the uploaded transactions are from Nationwide or not.
     */
    private void validateStatement(List<String> inputLines) {
        if(!inputLines.get(0).startsWith("\"Account Name:\"")) {
            throw new ReaderTypeMismatchException();
        }
    }
}
