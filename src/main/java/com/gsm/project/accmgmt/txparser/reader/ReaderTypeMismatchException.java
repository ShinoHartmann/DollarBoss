package com.gsm.project.accmgmt.txparser.reader;

/**
 * Thrown if the selected reader type and the uploaded bank statements do not match.
 */
public class ReaderTypeMismatchException extends RuntimeException {
}
