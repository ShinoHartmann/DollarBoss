package com.gsm.project.accmgmt.txparser;

/**
 * Thrown if no transaction categories could be found for a transaction.
 */
public class NoTransactionCategoryFoundException extends RuntimeException {

    private String txDescription;

    public NoTransactionCategoryFoundException(String txDescription) {
        this.txDescription = txDescription;
    }

    public String getTxDescription() {
        return txDescription;
    }
}
