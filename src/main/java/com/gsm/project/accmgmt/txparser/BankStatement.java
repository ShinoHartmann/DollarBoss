package com.gsm.project.accmgmt.txparser;

/**
 * JavaBean which stores bank statement information.
 */
public class BankStatement {

	private int userId;
	private String accountNumber;
	private String date;
	private String description;
	private String transactionCategory;
	private String amount;
	private String balance;

	public BankStatement(){}

	public BankStatement(int userId, String accountNumber, String date, String description, String transactionCategory, String amount) {
		this.userId = userId;
		this.accountNumber = accountNumber;
		this.date = date;
		this.description = description;
		this.transactionCategory = transactionCategory;
		this.amount = amount;
		this.balance = "0";
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getDate() {
		return date;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setTransactionCategory(String transactionCategory) {
		this.transactionCategory = transactionCategory;
	}
	
	public String getTransactionCategory() {
		return transactionCategory;
	}
	
	public void setAmount(String amount) {
		this.amount = amount.replaceAll("[^0-9.-]", "");
	}
	
	public String getAmount() {
		return amount;
	}
	
	public void setBalance(String balance) {
		this.balance = balance.replaceAll("[^0-9.-]", "");
	}
	
	public String getBalance() {
		return balance;
	}
	
	@Override
	public String toString() {
		String attributes = String.format("Account=%s; Date=%s; TransactionCategory=%s; Amount=%s; Description=%s; Balance=%s\n\n",
			getAccountNumber(),
			getDate(),
			getTransactionCategory(),
			getAmount(),
			getDescription(),
			getBalance()
		);
		return attributes;
	}
}