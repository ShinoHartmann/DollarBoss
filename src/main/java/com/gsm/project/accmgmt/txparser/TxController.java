package com.gsm.project.accmgmt.txparser;

import com.gsm.project.accmgmt.DbConnection;
import com.gsm.project.accmgmt.txparser.reader.*;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.*;

import static com.gsm.project.accmgmt.login.LoginController.USERID_SESSION_KEY;

/**
 * Controller handling account transactions.
 */
@Controller
public class TxController implements InitializingBean {

    @Autowired
    private ApplicationContext springContext;

    @Autowired
    private DbConnection dbConnection;

    private HashMap<String, Reader> readerList = new HashMap();

    /**
     * Stores all the available readers when the controller is initialized.
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        Map<String, Reader> readers = springContext.getBeansOfType(Reader.class);
        for(Reader reader : readers.values()) {
            readerList.put(reader.getType(), reader);
        }
    }

    /**
     * Directs to the Upload Statements page.
     */
    @RequestMapping("/uploadStatements")
    public String uploadStatements(Model model) throws SQLException {
        Set readerTypes = readerList.keySet();
        model.addAttribute("readerType", readerTypes);
        return "transactions/uploadStatements";
    }

    /**
     * Continues processing the transactions after a new transaction category has been added.
     */
    @RequestMapping("/continueParsing")
    public String continueParsing(HttpServletRequest request,
                                  @RequestParam("readerType") String readerType,
                                  @RequestParam("description") String description,
                                  @RequestParam("transactionCategory") String transactionCategory,
                                  Model model) throws Exception {
        HttpSession session = request.getSession();
        Integer userId = getUserId(session);
        String fileContent = (String) session.getAttribute("processingFile");
        Reader reader = readerList.get(readerType);
        int transactionCategoryId =  dbConnection.getTransactionCategoryId(userId, transactionCategory);
        dbConnection.storeDescription(userId, description, transactionCategoryId);
        return processFile(userId, fileContent, reader, model);
    }

    /**
     * Processes the uploaded transactions to store them in the database.
     * It checks the reader type and directs to the Upload Statements page if ReaderTypeMismatchException was thrown.
     */
    @RequestMapping(value="/processTransactions", method= RequestMethod.POST)
    public String processTransactions(HttpServletRequest request,
                                      @RequestParam("file") MultipartFile file,
                                      @RequestParam("readerType") String readerType,
                                      Model model) throws Exception {
        try {
            String fileContent = new String(file.getBytes());
            Reader reader = readerList.get(readerType);
            HttpSession session = request.getSession();
            session.setAttribute("processingFile", fileContent);
            Integer userId = getUserId(session);
            return processFile(userId, fileContent, reader, model);
        } catch(ReaderTypeMismatchException ex) {
            model.addAttribute("readerType", readerList.keySet());
            model.addAttribute("readerTypeMismatch", true);
            return "transactions//uploadStatements";
        }
    }

    /**
     * Processes the input file and shows a success page after all transactions have been processed.
     * If the account number from the transactions could not been found in the database
     * or no suitable account title was found, it shows the Add Account page
     * or Categorize Transaction page.
     */
    private String processFile(int userId, String fileContent,
                               Reader reader, Model model) throws Exception {
        Map<Integer, String> transactionCategories = dbConnection.getTransactionCategory(userId);
        try {
            flow(userId, fileContent, reader, model);
        } catch(AccountNotFoundException ex) {
            BankStatement bankStatement = ex.getBankStatement();
            model.addAttribute("accountNumber", bankStatement.getAccountNumber());
            model.addAttribute("reader", reader);
            return "transactions/addAccount";
        } catch(NoTransactionCategoryFoundException ex) {
            model.addAttribute("reader", reader.getType());
            model.addAttribute("transactionCategories", transactionCategories);
            model.addAttribute("description", ex.getTxDescription());
            return "transactions/categorizeTransaction";
        }
        return "transactions/success";
    }

    /**
     * Add transactions to the database.
     */
    private void flow(int userId, String file, Reader reader, Model model) throws Exception {
        List<String> lines = parseLines(file);
        int addCount = 0, duplicateCount = 0, errorCount = 0;
        String accountNumber = "";
        List<BankStatement> list = readStatementFile(userId, lines, reader);
        for(BankStatement bs : list) {
            if(!accountNumber.equals(bs.getAccountNumber())) {
                checkAccountExistence(userId, bs);
                accountNumber = bs.getAccountNumber();
            }
            try {
                dbConnection.storeBankStatement(bs);
                System.out.printf("Added:\n %s", bs);
                addCount++;
            } catch (Exception ex) {
                if(ex.getMessage().contains("Duplicate entry")) {
                    System.out.printf("DuplicateEntry:\n %s", bs);
                    duplicateCount++;
                } else {
                    System.out.printf("Error:\n %s \n%s", bs, ex.getMessage());
                    errorCount++;
                }
            }
        }

        model.addAttribute("addCount", addCount);
        model.addAttribute("duplicateCount", duplicateCount);
        model.addAttribute("errorCount", errorCount);
    }

    /**
     * Adds a new account if the account number from the transaction was not found in the database.
     */
    @RequestMapping("/addNewAccount")
    public String addNewAccount (HttpServletRequest request,
                                 @RequestParam("accountNumber") String accountNumber,
                                 @RequestParam("name") String name,
                                 @RequestParam("readerType") String readerType,
                                 Model model) throws Exception {

        HttpSession session = request.getSession();
        Integer userId = getUserId(session);
        String fileContent = (String) session.getAttribute("processingFile");
        dbConnection.storeNewAccount(userId, accountNumber, name);
        Reader reader = readerList.get(readerType);
        return processFile(userId, fileContent, reader, model);
    }

    /**
     * Directs to Add Transaction Manually page.
     */
    @RequestMapping("/addTransactionManually")
    public String addTransactionManually (HttpServletRequest request,
                                          Model model) throws Exception {
        HttpSession session = request.getSession();
        Integer userId = getUserId(session);
        Map<String, String> accounts = dbConnection.getAccountNumberAndName(userId);
        Map<Integer, String> transactionCategories = dbConnection.getTransactionCategory(userId);
        model.addAttribute("accounts", accounts);
        model.addAttribute("transactionCategories", transactionCategories);
        return "transactions/addTransactionManually";
    }

    /**
     * Adds a single transaction into the database.
     */
    @RequestMapping("/processTransactionManually")
    private String processTransactionManually(HttpServletRequest request,
                                              @RequestParam("accountNumber") String accountNumber,
                                              @RequestParam("date") String date,
                                              @RequestParam("description") String description,
                                              @RequestParam("transactionCategory") String transactionCategory,
                                              @RequestParam("amount") String amount,
                                              Model model) throws Exception {
        int addCount = 0, duplicateCount = 0, errorCount = 0;
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute(USERID_SESSION_KEY);
        BankStatement bankStatement = new BankStatement(
                userId,
                accountNumber,
                date,
                description,
                transactionCategory,
                amount
        );

        try {
            dbConnection.storeBankStatement(bankStatement);
            addCount++;
        } catch (Exception ex) {
            if(ex.getMessage().contains("Duplicate entry")) {
                System.out.printf("DuplicateEntry:\n %s", bankStatement);
                duplicateCount++;
            } else {
                System.out.printf("Error:\n %s \n%s", bankStatement, ex.getMessage());
                errorCount++;
            }
        }
        model.addAttribute("addCount", addCount);
        model.addAttribute("duplicateCount", duplicateCount);
        model.addAttribute("errorCount", errorCount);
        return "transactions/success";
    }

    /**
     * Splits the transactions input file into separate lines.
     */
    private List<String> parseLines(String file) {
        List<String> lines = new ArrayList<String>();
        String[] split = file.split("\n");
        for(String line : split) {
            lines.add(line);
        }
        return lines;
    }

    /**
     * Converts a list of transactions to a list of BankStatements.
     */
    public List<BankStatement> readStatementFile(int userId, List<String> inputLines, Reader reader) throws Exception {
        List<BankStatement> list = new ArrayList();
        reader.readFile(userId, inputLines, list);
        return list;
    }

    /**
     * Checks whether the account number is available in the database.
     */
    private void checkAccountExistence(int userId, BankStatement bankStatement) throws SQLException {
        String accountNumber = bankStatement.getAccountNumber();
        if(!accountNumber.equals(dbConnection.getAccountNumber(userId, accountNumber))) {
            throw new AccountNotFoundException(bankStatement);
        }
    }

    /**
     * @return current users id.
     */
    private Integer getUserId(HttpSession session) {
        return (Integer) session.getAttribute(USERID_SESSION_KEY);
    }
}