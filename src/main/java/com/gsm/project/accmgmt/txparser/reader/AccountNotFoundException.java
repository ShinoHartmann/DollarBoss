package com.gsm.project.accmgmt.txparser.reader;

import com.gsm.project.accmgmt.txparser.BankStatement;

/**
 * Thrown if the account was not found for a bank statement.
 */
public class AccountNotFoundException extends RuntimeException {

    private BankStatement bankStatement;

    public AccountNotFoundException(BankStatement bankStatement) {
        this.bankStatement = bankStatement;
    }

    public BankStatement getBankStatement() {
        return bankStatement;
    }
}
