package com.gsm.project.accmgmt.txparser.reader;

import com.gsm.project.accmgmt.txparser.BankStatement;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Santander specific debit statement reader.
 */
@Component
public class SantanderDebitReader extends Reader {

    public static final String TYPE = "Santander Debit";

    public SantanderDebitReader() {
        super(TYPE);
    }

    /**
     * @see Reader#readFile
     */
    @Override
    public void readFile(int userId, List<String> inputLines, List<BankStatement> list) throws Exception{
        validateStatement(inputLines);
        BankStatement stm = new BankStatement();
        String account = null;
        for(String line : inputLines) {
            if(line.startsWith("Account")) {
                account = extractAccount(line);
            } else if(line.startsWith("Date")) {
                stm = new BankStatement();
                String date = formatDate(parseValue(line), "ddMMyyyy");
                stm.setDate(date);
            } else if(line.startsWith("Description")) {
                stm.setDescription(parseValue(line));
            } else if(line.startsWith("Amount")) {
                stm.setAmount(parseValue(line).replaceAll("GBP",""));
                stm.setTransactionCategory(getTransactionCategory(userId, stm));
            } else if(line.startsWith("Balance")) {
                stm.setBalance(parseValue(line).replaceAll("GBP",""));
                list.add(stm);
                stm.setAccountNumber(account);
                stm.setUserId(userId);
            }
        }
    }

    /**
     * Validates whether the uploaded transactions are from a Santander debit card statement.
     */
    private void validateStatement(List<String> inputLines) {
        if(!inputLines.get(0).startsWith("From")) {
            throw new ReaderTypeMismatchException();
        }
    }
}
