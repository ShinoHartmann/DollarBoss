package com.gsm.project.accmgmt.txparser.reader;

import com.gsm.project.accmgmt.txparser.BankStatement;
import com.gsm.project.accmgmt.DbConnection;
import com.gsm.project.accmgmt.txparser.NoTransactionCategoryFoundException;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Abstract reader to read transactions from an input source.
 */
public abstract class Reader {

    @Autowired
    private DbConnection dbConnection;

    private String type;

    protected Reader(String type) {
        this.type = type;
    }

    /**
     * Gets a transaction category from a bank statement.
     * @return transaction category based on the description of the bank statement
     */
    protected String getTransactionCategory(int userId, BankStatement stm) throws Exception {
        Map<String, Integer> map = dbConnection.getDescriptionAndTransactionCategory(userId);
        String description = stm.getDescription();
        boolean found = false;
        String transactionCategory = null;
        for(String keyword : map.keySet()) {
            if(description.contains(keyword)) {
                int transactionCategoryId = map.get(keyword);
                transactionCategory = dbConnection.getTransactionCategory(userId, transactionCategoryId);
                found = true;
                break;
            }
        }
        if(!found) {
            throw new NoTransactionCategoryFoundException(description);
        }
        return transactionCategory.trim();
    }

    /**
     * Changes the original date to "yyyy-MM-dd" format.
     * @param oldDate original date
     * @param oldFormat original date format
     * @return newly formatted date
     */
    protected String formatDate(String oldDate, String oldFormat) throws Exception {
        SimpleDateFormat oldDateFormat = new SimpleDateFormat(oldFormat);
        Date date = oldDateFormat.parse(oldDate);
        SimpleDateFormat newDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return newDateFormat.format(date);
    }

    /**
     * Extract the account number from a transaction line.
     * @param line input line
     * @return Account number with four digits
     */
    protected String extractAccount(String line) throws Exception {
        return line.substring(line.length() - 4);
    }

    /**
     * Parses a field from the given input line.
     */
    protected static String parseValue(String line, String point, int index, String replacement, String replacedWith) {
        return line.split(point)[index].replaceAll(replacement, replacedWith).trim();
    }

    /**
     * Removes unnecessary characters from the input line.
     */
    protected static String parseValue(String line) {
        return parseValue(line, ":", 1, "[^\\w .,-]", "");
    }

    /**
     * Reads the transactions from the input and creates a BankStatement object for each line.
     * @param userId user who send the transactions
     * @param inputLines transaction source
     * @param list result list
     */
    public abstract void readFile(int userId, List<String> inputLines, List<BankStatement> list) throws Exception;

    /**
     * Returns name type of the reader.
     */
    public String getType() {
        return type;
    }
}
