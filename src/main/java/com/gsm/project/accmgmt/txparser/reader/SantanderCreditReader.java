package com.gsm.project.accmgmt.txparser.reader;

import com.gsm.project.accmgmt.txparser.BankStatement;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Santander specific credit statement reader.
 */
@Component
public class SantanderCreditReader extends Reader {

    public static final String TYPE = "Santander Credit";

    public SantanderCreditReader() {
        super(TYPE);
    }

    /**
     * @see Reader#readFile
     */
    @Override
    public void readFile(int userId, List<String> inputLines, List<BankStatement> list) throws Exception{
        validateStatement(inputLines);
        boolean foundStartLine = false;
        BankStatement stm;
        for(String line : inputLines) {
            if(line.startsWith("------")) {
                foundStartLine = true;
                continue;
            }
            String[] lineElement = line.split("\t\t"); //.replaceAll("\\s+", " ")
            if(foundStartLine) {
                if(lineElement[2].startsWith("INITIAL BALANCE")){
                    continue;
                } else if(!lineElement[1].equals("")) {
                    stm = new BankStatement();
                    stm.setUserId(userId);
                    stm.setAccountNumber(extractAccount(lineElement[1])); // Account
                    String amount = null;
                    stm.setDate(lineElement[0]); // Date
                    if(lineElement[3].equals("")) {
                        amount = "-" + lineElement[4];
                    } else {
                        amount = lineElement[3];
                    }
                    stm.setAmount(amount.replaceAll(",","")); // Amount
                    String description = parseValue(line, "\t\t", 2, "\\s+", " ");
                    stm.setDescription(String.format("%s %s, %s", description, stm.getDate(), stm.getAmount())); // Description
                    stm.setBalance("0"); // Balance
                    stm.setTransactionCategory(getTransactionCategory(userId, stm)); // Transaction Category
                    list.add(stm);
                }
            }
        }
    }

    /**
     * Validates whether the uploaded transactions are from a Santander credit card statement.
     */
    private void validateStatement(List<String> inputLines) {
        if(!inputLines.get(2).startsWith("----")) {
            throw new ReaderTypeMismatchException();
        }
    }
}
