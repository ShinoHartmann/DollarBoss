package com.gsm.project.accmgmt.login;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Prevents users to access the web page with a direct link without having been logging in.
 * Users are able to browse the pages after a session has been created.
 */
@Component
public class LoginInterceptor implements HandlerInterceptor {

    /**
     * Intercepts any requests to the application.
     * Prevents access to pages which need login.
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String path = request.getServletPath();
        if(areNotLoginPages(path)) {
            HttpSession session = request.getSession();
            if(isSessionNotCreated(session)) {
                throw new LoginRequiredException();
            }
        }
        return true;
    }

    /**
     * Checks if the requested page needs login.
     * @param path URL of the requested page
     * @return true if the requested page needs login
     */
    private boolean areNotLoginPages(String path) {
        return !path.startsWith("/login")
                && !path.startsWith("/processLogin")
                && !path.startsWith("/error");
    }

    /**
     * Checks whether the user has been already logged in to the application.
     */
    private boolean isSessionNotCreated(HttpSession session) {
        return session.isNew() || session.getAttribute("userId") == null;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }
}
