package com.gsm.project.accmgmt.login;

/**
 * Throws if the user couldn't be find in the system.
 */
public class UserNotFoundException extends RuntimeException{
    private String message;

    public UserNotFoundException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
