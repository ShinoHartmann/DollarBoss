package com.gsm.project.accmgmt.login;

/**
 * Exception thrown if the user requested a page which needs login.
 */
public class LoginRequiredException extends RuntimeException {
}
