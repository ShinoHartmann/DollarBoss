package com.gsm.project.accmgmt.login;

import com.gsm.project.accmgmt.DbConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;

/**
 * Controller which takes care of user login.
 * It creates a session when a user logs in to the application, then user ID and userName will be stored in the session.
*/
@Controller
public class LoginController {

    public static String USERID_SESSION_KEY = "userId";
    public static String USERNAME_SESSION_KEY = "userName";

    @Autowired
    private DbConnection dbConnection;

    /**
     * Directs to Home page
     */
    @RequestMapping("/home")
    public String home() throws SQLException {
        return "home";
    }

    /**
     * Directs to Login page
     */
    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    /**
     * Directs to Logout page
     */
    @RequestMapping("/logout")
    public String logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute(USERID_SESSION_KEY, null);
        return "logout";
    }

    /**
     * Checks whether the username and password matches
     * If it matches it stores user ID and userName in the session.
     */
    @RequestMapping("/processLogin")
    public void processLogin(HttpServletRequest request,
                             HttpServletResponse response,
                             @RequestParam("userName") String userName,
                             @RequestParam("inputPassword") String inputPassword) throws Exception {
        Integer userId = dbConnection.login(userName, inputPassword);
        HttpSession session = request.getSession();
        session.setAttribute(USERID_SESSION_KEY, userId);
        session.setAttribute(USERNAME_SESSION_KEY, userName);
        response.sendRedirect("/home");
    }
}
