package com.gsm.project.accmgmt;

import com.gsm.project.accmgmt.login.LoginRequiredException;
import com.gsm.project.accmgmt.login.UserNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Handler to show a suitable error page based on the thrown exception.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * Shows an error page if an unexpected exception is thrown.
     */
    @org.springframework.web.bind.annotation.ExceptionHandler(value = Exception.class)
    public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        e.printStackTrace();
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName("error");
        return mav;
    }

    /**
     * Shows the User Not Found page if a UserNotFoundException is thrown.
     */
    @org.springframework.web.bind.annotation.ExceptionHandler(value = UserNotFoundException.class)
    public ModelAndView userNotFoundErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName("userNotFound");
        return mav;
    }

    /**
     * Shows the login page if a LoginRequiredException is thrown.
     */
    @org.springframework.web.bind.annotation.ExceptionHandler(value = LoginRequiredException.class)
    public void loginRequired(HttpServletResponse response) throws Exception {
        response.sendRedirect("/login");
    }
}
