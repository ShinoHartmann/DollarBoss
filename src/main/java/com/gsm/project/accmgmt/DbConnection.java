package com.gsm.project.accmgmt;

import com.gsm.project.accmgmt.login.UserNotFoundException;
import com.gsm.project.accmgmt.txparser.BankStatement;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.*;
import java.util.*;

/**
 * Handles access to the database.
 */
@Component
public class DbConnection implements InitializingBean {

	@Value("${database.driver}") private String databaseDriver;
	@Value("${database.url}") private String databaseUrl;
	@Value("${database.username}") private String databaseUsername;
	@Value("${database.password}") private String databasePassword;

    private Connection connection;

    /**
     * Connects to the database.
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        connection = DriverManager.getConnection(databaseUrl + "?user=" + databaseUsername + "&password=" + databasePassword);
    }

    /**
     * Persists a description and an account title.
     */
    public void storeDescription(int userId, String description, int transactionCategory) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "INSERT into description"
                        + " (user_id, description, transaction_category)"
                        + " VALUES (?, ?, ?)");
        preparedStatement.setInt(1, userId);
        preparedStatement.setString(2, description);
        preparedStatement.setInt(3, transactionCategory);
        executeStmt(preparedStatement);
    }

    /**
     * Persists a bank statement.
     */
	public void storeBankStatement(BankStatement bankStatement) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "INSERT into details"
                        + " (user_id, account, txdate, transaction_category, amount, description, balance)"
                        + " VALUES (?, ?, ?, ?, ?, ?, ?)");
        preparedStatement.setInt(1, bankStatement.getUserId());
        preparedStatement.setString(2, bankStatement.getAccountNumber());
        preparedStatement.setString(3, bankStatement.getDate());
        preparedStatement.setString(4, bankStatement.getTransactionCategory());
        preparedStatement.setString(5, bankStatement.getAmount());
        preparedStatement.setString(6, bankStatement.getDescription());
        preparedStatement.setString(7, bankStatement.getBalance());
        executeStmt(preparedStatement);
	}

    /**
     * Executes a SQL statement.
     */
    private void executeStmt(PreparedStatement preparedStatement) throws SQLException {
        try {
            preparedStatement.executeUpdate();
        } finally {
            preparedStatement.close();
        }
    }

    /**
     * Gets a user ID based on the username and the password.
     */
    public int login(String username, String password) throws SQLException {
        int userId = 0;
        String selectSQL = "SELECT user_id from users where username ='" + username
                + "' AND pwd = PASSWORD('" + password + "')";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(selectSQL);
        if(resultSet.next()) {
            userId = resultSet.getInt("user_id");
        } else {
            throw new UserNotFoundException("Incorrect username or password");
        }
        return userId;
    }

    /**
     * Gets all the transaction categories from the database.
     */
    public  Map<Integer, String> getTransactionCategory(int userId) throws SQLException {
        Map<Integer, String> map = new HashMap<Integer, String>();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM transaction_category where user_id =" + userId + " ORDER BY category asc");
        while(resultSet.next()){
            Integer id = resultSet.getInt("id");
            String transactionCategory = resultSet.getString("category");
            map.put(id, transactionCategory);
        }
        return map;
    }

    /**
     * Gets all the keywords and the related account titles from the database.
     */
    public Map<String, Integer> getDescriptionAndTransactionCategory(int userId) throws SQLException {
        Map<String, Integer> map = new HashMap();
        String selectSQL = "SELECT * FROM description WHERE user_id = " + userId + " ORDER BY length(description) desc";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(selectSQL);

        while(resultSet.next()){
            String description = resultSet.getString("description");
            int transactionCategory = resultSet.getInt("transaction_category");
            map.put(description, transactionCategory);
        }
        return map;
    }

    /**
     *  Gets the transaction category based on the transaction category id and the user id.
     */
    public String getTransactionCategory(int userId, int id) throws SQLException{
        String selectSQL = "SELECT category FROM transaction_category where id = " + id
                + " AND user_id = " + userId;
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(selectSQL);
        if(resultSet.next()) {
            return resultSet.getString("category");
        }
        return null;
    }

    /**
     * Checks if the account number exists in the database.
     * @return account number if found, returns null if not found.
     */
    public String getAccountNumber(int userId, String accountNumber) throws SQLException {
        String selectSQL = "SELECT account_no FROM `account` where account_no = '"
         + accountNumber + "' AND user_id = " + userId;
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(selectSQL);
        if(resultSet.next()) {
            return resultSet.getString("account_no");
        } else {
            return null;
        }
    }

    /**
     * Gets all the account numbers and the corresponding account name.
     */
    public Map<String, String> getAccountNumberAndName(int userId) throws SQLException{
        Map<String, String> map = new HashMap();
        String selectSQL = "SELECT account_no, `name` FROM `account` where user_id = " + userId;
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(selectSQL);
        while(resultSet.next()){
            String accountNumber = resultSet.getString("account_no");
            String accountName = resultSet.getString("name");
            map.put(accountNumber, accountName);
        }
        return map;
    }

    /**
     * Persists a new transaction category.
     */
    public void storeNewTransactionCategory(int userId, String transactionCategory) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "INSERT into transaction_category"
                        + " (user_id, category)"
                        + " VALUES (?, ?)");
        preparedStatement.setInt(1, userId);
        preparedStatement.setString(2, transactionCategory);
        executeStmt(preparedStatement);
    }

    /**
     * Gets the ID of the given transaction category.
     */
    public int getTransactionCategoryId(int userId, String transactionCategory) throws SQLException{
        String selectSQL = "SELECT id FROM transaction_category" +
                " WHERE category = '" + transactionCategory +
                "' AND user_id = " + userId;
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(selectSQL);
        if(resultSet.next()) {
            return resultSet.getInt("id");
        } else {
            storeNewTransactionCategory(userId, transactionCategory);
            return getTransactionCategoryId(userId, transactionCategory);
        }
    }

    /**
     * Gets all the transactions from the database.
     */
    public List<BankStatement> getAllDetails(int userId) throws SQLException {
        List<BankStatement> bankStatements = new ArrayList<BankStatement>();
        String selectSQL = "SELECT * FROM details" +
                " WHERE user_id = " + userId +
                " ORDER BY txdate DESC";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(selectSQL);
        while(resultSet.next()){
            BankStatement bs = new BankStatement();
            bs.setUserId(userId);
            bs.setAccountNumber(resultSet.getString("account"));
            bs.setDate(resultSet.getString("txdate"));
            bs.setTransactionCategory(resultSet.getString("transaction_category"));
            bs.setAmount(resultSet.getString("amount"));
            bs.setDescription(resultSet.getString("description"));
            bs.setBalance(resultSet.getString("balance"));
            bankStatements.add(bs);
        }
        return bankStatements;
    }

    /**
     * Gets the total amounts of all transaction categories per month.
     */
    public Map<String, Map<String, BigDecimal>> getMonthlySummary(int userId) throws SQLException{
        Map<String, Map<String, BigDecimal>> monthlySummary = new TreeMap();
        String selectSQL = "SELECT LEFT(txdate, 7) AS groupDate, transaction_category, SUM(amount) as amount" +
                " FROM details " +
                " WHERE user_id = " + userId +
                " GROUP BY groupDate, transaction_category " +
                " ORDER BY groupDate DESC;";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(selectSQL);
        while(resultSet.next()) {
            String monthAndYear = resultSet.getString("groupDate");
            String category = resultSet.getString("transaction_category");
            BigDecimal amount = new BigDecimal(resultSet.getString("amount"));
            if(monthlySummary.get(monthAndYear) == null) {
                Map<String, BigDecimal> categoryAndAmount = new HashMap();
                monthlySummary.put(monthAndYear, categoryAndAmount);
            }
            monthlySummary.get(monthAndYear).put(category, amount);
        }
        return monthlySummary;
    }

    /**
     * Gets the monthly expenses of the given transaction category of the past 12 month.
     */
    public Map<String, BigDecimal> getYearlyExpensesByTransactionCategory(
            int userId,
            String transactionCategory) throws SQLException{

        Map<String, BigDecimal> yearlyExpense = new TreeMap();
        String selectSQL = "SELECT LEFT(txdate, 7) as groupDate, SUM(amount) as expense FROM details" +
                " WHERE user_id = " + userId +
                " AND transaction_category = '" + transactionCategory + "'"+
                " GROUP BY groupDate" +
                " ORDER BY groupDate DESC LIMIT 12";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(selectSQL);
        while(resultSet.next()){
            String month = resultSet.getString("groupDate");
            BigDecimal amount = new BigDecimal(resultSet.getString("expense")).setScale(2, BigDecimal.ROUND_HALF_UP);
            yearlyExpense.put(month, amount);
        }
        return yearlyExpense;
    }

    /**
     * Persists a new account.
     */
    public void storeNewAccount(int userId, String accountNo, String name) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "INSERT INTO `account`"
                        + " (account_no, user_id, `name`) "
                        + " VALUES (?, ?, ?)");
        preparedStatement.setString(1, accountNo);
        preparedStatement.setInt(2, userId);
        preparedStatement.setString(3, name);
        executeStmt(preparedStatement);
    }
}