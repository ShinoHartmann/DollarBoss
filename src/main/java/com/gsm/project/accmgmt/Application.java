package com.gsm.project.accmgmt;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * SpringBoot application initializer.
 *
 * The @EnableAutoConfiguration annotation switches on reasonable default behaviors
 * based on the content of your classpath. For example, because the application depends on the
 * embeddable version of Tomcat (tomcat-embed-core.jar), a Tomcat server is set up and configured
 * with reasonable defaults on your behalf. And because the application also depends on Spring MVC (spring-webmvc.jar),
 * a Spring MVC DispatcherServlet is configured and registered for you — no web.xml necessary!
 */
@Configuration
@ComponentScan // tells Spring to search recursively through the package and its children for classes marked with Spring’s @Component
@EnableAutoConfiguration
public class Application extends SpringBootServletInitializer {

	@Value("${keystore.file}") private String keystoreFile;
	@Value("${keystore.pass}") private String keystorePass;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	/**
	 * Executes the web application in an embedded tomcat.
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	/**
	 * Configures the web container.
	 */
	@Bean
	public EmbeddedServletContainerCustomizer containerCustomizer() throws Exception {
		return new EmbeddedServletContainerCustomizer() {
			@Override
			public void customize(ConfigurableEmbeddedServletContainer factory) {
				if (factory instanceof TomcatEmbeddedServletContainerFactory) {
					TomcatEmbeddedServletContainerFactory containerFactory = (TomcatEmbeddedServletContainerFactory) factory;
					containerFactory.addConnectorCustomizers(new TomcatConnectorCustomizer() {
						@Override
						public void customize(Connector connector) {
							customizeSslConnector(connector);
						}
					});
				}
			}
		};
	}

	/**
	 * Enables SSL.
	 */
	private Connector customizeSslConnector(Connector connector) {
			connector.setPort(8443);
			connector.setSecure(true);
			connector.setScheme("https");
			Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
			protocol.setSSLEnabled(true);
			protocol.setKeystoreFile(getKeyStoreFile().getAbsolutePath());
			protocol.setKeystorePass(keystorePass);
			protocol.setKeystoreType("PKCS12");
			protocol.setKeyAlias("tomcat");
			return connector;
	}

	/**
	 * Returns the key store file which contains the applications certificate.
	 */
	private File getKeyStoreFile() {
		ClassPathResource resource = new ClassPathResource(keystoreFile);
		try {
			return resource.getFile();
		} catch (Exception ex) {
			File temp = null;
			try {
				temp = File.createTempFile("keystore", ".tmp");
				FileCopyUtils.copy(resource.getInputStream(), new FileOutputStream(temp));
			} catch(IOException ioEx) {
				throw new RuntimeException("keystore problem", ioEx);
			}
			return temp;
		}
	}
}
