package com.gsm.project.accmgmt.Statistics;

import com.gsm.project.accmgmt.txparser.BankStatement;
import com.gsm.project.accmgmt.DbConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.gsm.project.accmgmt.login.LoginController.USERID_SESSION_KEY;

/**
 * Controller which takes care of transaction statistics.
 */
@Controller
public class StatisticsController {

    @Autowired
    private DbConnection dbConnection;

    /**
     * Gets all the statements stored in the database.
     * @return statements in JSON format
     */
    @RequestMapping("/getDetails")
    public @ResponseBody DataTableCollection getDetails(HttpServletRequest request) throws Exception {
        Integer userId = getUserId(request);
        List<BankStatement> statements = dbConnection.getAllDetails(userId);
        return new DataTableCollection(statements);
    }

    /**
     * Directs to the Show Monthly Summary page.
     */
    @RequestMapping("/showMonthlySummary")
    public String showMonthlySummary(HttpServletRequest request) throws Exception{
        return "statistics/showMonthlySummary";
    }

    /**
     * Gets the monthly summary of the logged in user.
     * @return monthly summary in JSON format
     */
    @RequestMapping("/getMonthlySummary")
    public @ResponseBody Map<String, Object> getMonthlySummary(HttpServletRequest request) throws Exception {
        Integer userId = getUserId(request);
        Map<String, Map<String, BigDecimal>> monthlySummary = dbConnection.getMonthlySummary(userId);
        List<Map<String, String>> dataList = new ArrayList<Map<String, String>>();
        for(String month: monthlySummary.keySet()) {
            Map<String, BigDecimal> categoryAndAmounts = monthlySummary.get(month);
            addMonthlySummaryToDatalist(dataList, month, categoryAndAmounts);
        }
        Map<Integer, String> transactionCategories = dbConnection.getTransactionCategory(userId);
        List<Map<String, String>> columns = new ArrayList<Map<String, String>>();
        addColumns(transactionCategories, columns);
        fillMissingCategories(dataList, transactionCategories);
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("data", dataList);
        result.put("columns", columns);
        return result;
    }

    /**
     * Add the given account titles to the given column map.
     */
    private void addColumns(Map<Integer, String> transactionCategories, List<Map<String, String>> columns) {
        Map<String, String> columnMeta = new HashMap<String, String>();
        columnMeta.put("data", "Month");
        columnMeta.put("title", "Month");
        columns.add(columnMeta);
        for(String column : transactionCategories.values()) {
            columnMeta = new HashMap<String, String>();
            columnMeta.put("data", column);
            columnMeta.put("title", column);
            columns.add(columnMeta);
        }
        columnMeta.put("data", "Savings");
        columnMeta.put("title", "Savings");
    }

    /**
     * Calculate the total expense of every account title per month into the result list.
     */
    private void addMonthlySummaryToDatalist(List<Map<String, String>> dataList,
                                             String month,
                                             Map<String, BigDecimal> categoryAndAmounts) {
        Map<String, String> labelAndData = new HashMap<String, String>();
        labelAndData.put("Month", month);
        BigDecimal total = new BigDecimal(0);
        for(String transactionCategory : categoryAndAmounts.keySet()) {
            BigDecimal amount = categoryAndAmounts.get(transactionCategory).setScale(2, BigDecimal.ROUND_HALF_UP);
            total = total.add(amount);
            String formattedAmount = amount.toString();
            labelAndData.put(transactionCategory, formattedAmount);
        }
        labelAndData.put("Savings", total.toString());
        dataList.add(labelAndData);
    }

    /**
     * Presets zero amount to every category if there is no transaction available for the category.
     */
    private void fillMissingCategories(List<Map<String, String>> table, Map<Integer, String> transactionCategories) {
        for(Map<String, String> monthlyValues : table) {
            for(String category : transactionCategories.values()) {
                if(!monthlyValues.containsKey(category)) {
                    monthlyValues.put(category, "0");
                }
            }
        }
    }

    /**
     * Directs to the Show Yearly Statistics page.
     */
    @RequestMapping("/showYearlyStatisticsByCategory")
    public String showYearlyStatisticsByCategory(HttpServletRequest request, Model model) throws Exception{
        Integer userId = getUserId(request);
        Map<Integer, String> transactionCategories = dbConnection.getTransactionCategory(userId);
        model.addAttribute("transactionCategories", transactionCategories.values());
        return "statistics/showYearlyStatisticsByCategory";
    }

    /**
     * Gets yearly expense and averages within the previous 12 month.
     * @return yearly expense in JSON format
     */
    @RequestMapping("/getYearlyExpenses")
    public @ResponseBody List<List> getYearlyExpensesByCategory(HttpServletRequest request,
                                                                @RequestParam String category) throws Exception {
        Integer userId = getUserId(request);
        Map<String, BigDecimal> yearlyExpenses = dbConnection.getYearlyExpensesByTransactionCategory(userId, category);
        List<List> list = new ArrayList<List>();
        List<String> months = new ArrayList<String>(yearlyExpenses.keySet());
        List<BigDecimal> expenses = new ArrayList<BigDecimal>(yearlyExpenses.values());
        BigDecimal total = new BigDecimal(0);
        int numerator = 0;
        for(BigDecimal monthlyExpense: expenses) {
            total = total.add(monthlyExpense);
            numerator++;
        }
        // calculate and add average expense of the category.
        BigDecimal average = new BigDecimal("0");
        if(total.compareTo(BigDecimal.ZERO) != 0) {
            average = total.divide(new BigDecimal(numerator), 2, RoundingMode.HALF_UP);
        }
        List<BigDecimal> averages = new ArrayList<BigDecimal>();
        // add average in the list as many as the number of the month of the
        for(int i = 0; i < expenses.size(); i++) {
            averages.add(average);
        }
        list.add(months);
        list.add(expenses);
        list.add(averages);
        return list;
    }

    /**
     * @return user ID which is stored in the session
     */
    private Integer getUserId(HttpServletRequest request) {
        HttpSession session = request.getSession();
        return (Integer) session.getAttribute(USERID_SESSION_KEY);
    }
}
