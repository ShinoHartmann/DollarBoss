package com.gsm.project.accmgmt.Statistics;

import java.util.List;

/**
 * Container to store data to generate the correct JSON format needed by the DataTable UI component.
 */
public class DataTableCollection {

    private final List data;

    public DataTableCollection(List data) {
        this.data = data;
    }

    public List getData() {
        return data;
    }
}
